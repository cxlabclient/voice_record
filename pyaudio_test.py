import pyaudio
import wave
import datetime
import sounddevice
import base64
import requests
import os


# info = sounddevice.query_devices()
# print(info)
#
microphone_id = 0
p = pyaudio.PyAudio()
for i in range(p.get_device_count()):
    if p.get_device_info_by_index(i)['name'] == 'default':
        microphone_id = p.get_device_info_by_index(i)['index']
        break

class Record:
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    RECORD_SECONDS = 3 * 60

    pa = pyaudio.PyAudio()

    def __init__(self):
        self.WAVE_OUTPUT_FILENAME = str(int(datetime.datetime.now().timestamp())) + "_output.wav"
        self.cancel_requested = False
        self.frames = list()

    def start_record(self):
        self.stream = self.pa.open(format=self.FORMAT,
                                   channels=self.CHANNELS,
                                   rate=self.RATE,
                                   input=True,
                                   input_device_index=self.get_input_device_index(),
                                   frames_per_buffer=self.CHUNK)
        print("* start recording")
        try:
            for i in range(0, int(self.RATE / self.CHUNK * self.RECORD_SECONDS)):
                if self.cancel_requested:
                    break
                data = self.stream.read(self.CHUNK)
                self.frames.append(data)
            print("* done recording")
            self.stream.stop_stream()
        finally:
            # self.stream.close()
            # self.pa.terminate()
            self.pa.close(self.stream)
            self.create_file()
            send_file(self.WAVE_OUTPUT_FILENAME)

    def create_file(self):
        with wave.open(self.WAVE_OUTPUT_FILENAME, 'wb') as wf:
            wf.setnchannels(self.CHANNELS)
            wf.setsampwidth(self.pa.get_sample_size(self.FORMAT))
            wf.setframerate(self.RATE)
            wf.writeframes(b''.join(self.frames))

    def get_input_device_index(self):
        for i in range(self.pa.get_device_count()):
            if p.get_device_info_by_index(i)['name'] == 'default':
                return p.get_device_info_by_index(i)['index']


def convert_file_to_base64(file_name):

    with open(file_name, 'rb') as file:
        file_content = file.read()
    return base64.b64encode(file_content)


def send_file(file_name):
    url = 'http://127.0.0.1:5000'
    r = requests.post(url=url, data={'file': convert_file_to_base64(file_name)})
    # remove_file(file_name)


def remove_file(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)


for file_name in os.listdir():
    if file_name.split(".")[-1] == 'wav':
        send_file(file_name)


