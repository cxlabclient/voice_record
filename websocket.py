import asyncio
import json
import logging
import websockets
import pyaudio_test
import threading

logging.basicConfig()


async def turn_microphone_on(websocket):
    r = pyaudio_test.Record()
    print('********************************', r)
    my_thread = threading.Thread(target=r.start_record)
    my_thread.start()
    await websocket.send(json.dumps({"status": "ON"}))
    return r


async def turn_microphone_off(websocket, recorder):
    if recorder is not None:
        recorder.cancel_requested = True
        await websocket.send(json.dumps({"status": "OFF"}))


async def handler(websocket, path):
    recorder = None
    try:
        async for message in websocket:
            data = json.loads(message)
            if data["action"] == "plus":
                recorder = await turn_microphone_on(websocket)
                print("plus", threading.active_count())
            elif data["action"] == "minus":
                await turn_microphone_off(websocket, recorder)
                recorder = None
                print("minus", threading.active_count())
            else:
                logging.error("unsupported event: {}", data)
    except:
        if recorder is not None:
            recorder.cancel_requested = True


# start_server = websockets.serve(counter, "localhost", 30303)
start_server = websockets.serve(handler, "localhost", 30303)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()


# STATE = {"value": 0}
#
# USERS = set()


# def state_event():
#     return json.dumps({"type": "state", **STATE})
#
#
# def users_event():
#     return json.dumps({"type": "users", "count": len(USERS)})


# async def notify_state():
#     if USERS:  # asyncio.wait doesn't accept an empty list
#         message = state_event()
#         await asyncio.wait([user.send(message) for user in USERS])


# async def notify_users():
#     if USERS:  # asyncio.wait doesn't accept an empty list
#         message = users_event()
#         await asyncio.wait([user.send(message) for user in USERS])


# async def register(websocket):
#     USERS.add(websocket)
#     await notify_users()


# async def unregister(websocket):
#     USERS.remove(websocket)
#     await notify_users()





# async def consumer_handler(websocket, path):
#     async for message in websocket:
#         await consumer(message)
#
#
# async def producer_handler(websocket, path):
#     while True:
#         message = await producer()
#         await websocket.send(message)
#
#
# async def handler(websocket, path):
#     consumer_task = asyncio.ensure_future(
#         consumer_handler(websocket, path))
#     producer_task = asyncio.ensure_future(
#         producer_handler(websocket, path))
#     done, pending = await asyncio.wait(
#         [consumer_task, producer_task],
#         return_when=asyncio.FIRST_COMPLETED,
#     )
#     for task in pending:
#         task.cancel()