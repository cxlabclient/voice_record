from flask import Flask, request
import base64
import datetime
import wave
import threading
import os
import time

today = datetime.datetime.now()

app = Flask(__name__)
HOW_LONG_SHOULD_FILE_CAN_LIVE = 20  # days
HOW_OFTEN_CLEAN_THE_SERVER = 60*60*24 # seconds


@app.route('/', methods=['POST'])
def index():
    file_as_str = request.values.get('file')
    file_name = str(int(datetime.datetime.now().timestamp())) + "_inserver_output.wav"
    with open(file_name, 'wb') as file:
        file.write(base64.b64decode(file_as_str))
    return "OK"


def schedule_removing_old_files(interval):
    # for file_name in os.listdir():
    #     if file_name.split(".")[-1] == 'wav':
    #         with open(file_name, 'rb') as file:
    #             print(dir(file))
    while True:
        with os.scandir() as dir_entries:
            files_to_remove = list(filter(lambda a: a.is_file()
                               and a.name.split(".")[-1] == 'wav'
                               and (today - datetime.datetime.fromtimestamp(a.stat().st_ctime)).days < HOW_LONG_SHOULD_FILE_CAN_LIVE
                               , dir_entries))
        for file in files_to_remove:
            if os.path.exists(file.path):
                os.remove(file.path)
        time.sleep(interval)


if __name__ == '__main__':
    my_thread = threading.Thread(target=schedule_removing_old_files, args=(HOW_OFTEN_CLEAN_THE_SERVER,))
    my_thread.start()
    app.run()
