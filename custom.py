# import base64
#
# # file_content = None
# with open('111.ogg', 'rb') as file:
#     file_name = file.name
#     print(file_name)
#     file_content = file.read()
# result = base64.b64encode(file_content)
#
# with open("../"+file_name, "wb") as new_file:
#     new_file.write(base64.decodebytes(result))
# print(result)

# import os
#
# from datetime import datetime
#
# with os.scandir() as dir_entries:
#     for entry in dir_entries:
#         info = entry.stat()
#         timestamp = info.st_mtime
#         print(entry.name, ' -> ', datetime.fromtimestamp(timestamp))

# for file_name in os.listdir('../'):
#     if file_name.split(".")[-1] == 'ogg':
#         with open(file_name, 'rb') as file:
#             print(file.stat())



# check for disk space
# import shutil
#
# total, used, free = shutil.disk_usage("/")
#
# print("Total: %d GiB" % (total // (2**30)))
# print("Used: %d GiB" % (used // (2**30)))
# print("Free: %d GiB" % (free // (2**30)))